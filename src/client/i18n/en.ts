<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.0" language="en">
<context>
    <name>AddContacts</name>
    <message>
        <location filename="../UI/Groups/AddContacts.qml" line="192"/>
        <source>Add contacts</source>
        <translation>Add contacts</translation>
    </message>
    <message>
        <location filename="../UI/Groups/AddContacts.qml" line="224"/>
        <source>Quick search</source>
        <translation>Quick search</translation>
    </message>
    <message>
        <location filename="../UI/Groups/AddContacts.qml" line="291"/>
        <source>No contacts yet. Try to resync</source>
        <translation>No contacts yet. Try to resync</translation>
    </message>
    <message>
        <location filename="../UI/Groups/AddContacts.qml" line="349"/>
        <source>Done</source>
        <translation>Done</translation>
    </message>
</context>
<context>
    <name>BigProfileImage</name>
    <message>
        <location filename="../UI/common/BigProfileImage.qml" line="26"/>
        <source>Picture</source>
        <translation>Picture</translation>
    </message>
</context>
<context>
    <name>BubbleDelegate</name>
    <message>
        <location filename="../UI/Conversations/BubbleDelegate.qml" line="88"/>
        <location filename="../UI/Conversations/BubbleDelegate.qml" line="98"/>
        <location filename="../UI/Conversations/BubbleDelegate.qml" line="121"/>
        <source>Today</source>
        <translation>Today</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/BubbleDelegate.qml" line="88"/>
        <location filename="../UI/Conversations/BubbleDelegate.qml" line="98"/>
        <location filename="../UI/Conversations/BubbleDelegate.qml" line="121"/>
        <source>Yesterday</source>
        <translation>Yesterday</translation>
    </message>
</context>
<context>
    <name>CapturePreview</name>
    <message>
        <location filename="../UI/Conversations/CapturePreview.qml" line="83"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/CapturePreview.qml" line="97"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
</context>
<context>
    <name>ChangeStatus</name>
    <message>
        <location filename="../UI/ChangeStatus/ChangeStatus.qml" line="50"/>
        <source>Change status</source>
        <translation>Change status</translation>
    </message>
    <message>
        <location filename="../UI/ChangeStatus/ChangeStatus.qml" line="106"/>
        <source>Enter new status</source>
        <translation>Enter new status</translation>
    </message>
    <message>
        <location filename="../UI/ChangeStatus/ChangeStatus.qml" line="149"/>
        <source>Done</source>
        <translation>Done</translation>
    </message>
</context>
<context>
    <name>ChangeSubject</name>
    <message>
        <location filename="../UI/Groups/ChangeSubject.qml" line="50"/>
        <source>Change subject</source>
        <translation>Change subject</translation>
    </message>
    <message>
        <location filename="../UI/Groups/ChangeSubject.qml" line="71"/>
        <source>Enter new subject:</source>
        <translation>Enter new subject:</translation>
    </message>
    <message>
        <location filename="../UI/Groups/ChangeSubject.qml" line="114"/>
        <source>Done</source>
        <translation>Done</translation>
    </message>
</context>
<context>
    <name>Chat</name>
    <message>
        <location filename="../UI/Chats/Chat.qml" line="37"/>
        <source>Fetching group subject</source>
        <translation>Fetching group subject</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chat.qml" line="105"/>
        <source>You</source>
        <translation>You</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chat.qml" line="285"/>
        <source>%1 joined the group</source>
        <translation>%1 joined the group</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chat.qml" line="286"/>
        <source>%1 left the group</source>
        <translation>%1 left the group</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chat.qml" line="288"/>
        <location filename="../UI/Chats/Chat.qml" line="289"/>
        <source>%1 changed the subject to %2</source>
        <translation>%1 changed the subject to %2</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chat.qml" line="291"/>
        <location filename="../UI/Chats/Chat.qml" line="292"/>
        <source>%1 changed the group picture</source>
        <translation>%1 changed the group picture</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chat.qml" line="293"/>
        <source>(no messages)</source>
        <translation>(no messages)</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chat.qml" line="304"/>
        <source>is writing a message...</source>
        <translation>is writing a message...</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chat.qml" line="315"/>
        <source>Today</source>
        <translation>Today</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chat.qml" line="315"/>
        <source>Yesterday</source>
        <translation>Yesterday</translation>
    </message>
</context>
<context>
    <name>Chats</name>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="150"/>
        <source>Chats</source>
        <translation>Chats</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="172"/>
        <source>No conversations yet</source>
        <translation>No conversations yet</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="205"/>
        <source>Delete and exit group</source>
        <translation>Delete and exit group</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="205"/>
        <source>Delete Conversation</source>
        <translation>Delete Conversation</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="239"/>
        <source>View group information</source>
        <translation>View group information</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="239"/>
        <source>View contact profile</source>
        <translation>View contact profile</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="253"/>
        <source>Confirm Delete</source>
        <translation>Confirm Delete</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="254"/>
        <source>Are you sure you want to delete this conversation and all its messages?</source>
        <translation>Are you sure you want to delete this conversation and all its messages?</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="255"/>
        <location filename="../UI/Chats/Chats.qml" line="267"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="256"/>
        <location filename="../UI/Chats/Chats.qml" line="268"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="265"/>
        <source>Confirm Delete and Exit</source>
        <translation>Confirm Delete and Exit</translation>
    </message>
    <message>
        <location filename="../UI/Chats/Chats.qml" line="266"/>
        <source>Are you sure you want to delete this conversation and exit this group?</source>
        <translation>Are you sure you want to delete this conversation and exit this group?</translation>
    </message>
</context>
<context>
    <name>ContactProfile</name>
    <message>
        <location filename="../UI/Contacts/ContactProfile.qml" line="66"/>
        <location filename="../UI/Contacts/ContactProfile.qml" line="248"/>
        <source>Unknown contact</source>
        <translation>Unknown contact</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/ContactProfile.qml" line="173"/>
        <source>Update status</source>
        <translation>Update status</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/ContactProfile.qml" line="185"/>
        <source>Phone:</source>
        <translation>Phone:</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/ContactProfile.qml" line="207"/>
        <source>Mobile phone</source>
        <translation>Mobile phone</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/ContactProfile.qml" line="255"/>
        <source>Add to contacts</source>
        <translation>Add to contacts</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/ContactProfile.qml" line="265"/>
        <source>Contact blocked</source>
        <translation>Contact blocked</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/ContactProfile.qml" line="279"/>
        <source>Block contact</source>
        <translation>Block contact</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/ContactProfile.qml" line="279"/>
        <source>Unblock contact</source>
        <translation>Unblock contact</translation>
    </message>
</context>
<context>
    <name>Contacts</name>
    <message>
        <location filename="../UI/Contacts/Contacts.qml" line="160"/>
        <source>Contacts</source>
        <translation>Contacts</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/Contacts.qml" line="228"/>
        <source>Quick search</source>
        <translation>Quick search</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/Contacts.qml" line="277"/>
        <source>No contacts yet. Try to resync</source>
        <translation>No contacts yet. Try to resync</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/Contacts.qml" line="332"/>
        <source>Block contact</source>
        <translation>Block contact</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/Contacts.qml" line="332"/>
        <source>Unblock contact</source>
        <translation>Unblock contact</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/Contacts.qml" line="343"/>
        <source>View contact profile</source>
        <translation>View contact profile</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/Contacts.qml" line="373"/>
        <source>Confirm delete</source>
        <translation>Confirm delete</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/Contacts.qml" line="374"/>
        <source>Are you sure you want to delete %1?</source>
        <translation>Are you sure you want to delete %1?</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/Contacts.qml" line="375"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/Contacts.qml" line="376"/>
        <source>No</source>
        <translation>No</translation>
    </message>
</context>
<context>
    <name>ContactsGroupsSelector</name>
    <message>
        <location filename="../UI/Selectors/ContactsGroupsSelector.qml" line="54"/>
        <source>Contacts</source>
        <translation>Contacts</translation>
    </message>
    <message>
        <location filename="../UI/Selectors/ContactsGroupsSelector.qml" line="61"/>
        <source>Groups</source>
        <translation>Groups</translation>
    </message>
</context>
<context>
    <name>Conversation</name>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="68"/>
        <source>Fetching group subject</source>
        <translation>Fetching group subject</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="410"/>
        <source>Back</source>
        <translation>Back</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="497"/>
        <source>You</source>
        <translation>You</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="621"/>
        <source>Loading conversation...</source>
        <translation>Loading conversation...</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="778"/>
        <source>Contact blocked</source>
        <translation>Contact blocked</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="779"/>
        <source>Write your message here</source>
        <translation>Write your message here</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="836"/>
        <location filename="../UI/Conversations/Conversation.qml" line="1011"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="1032"/>
        <source>Copy content</source>
        <translation>Copy content</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="1044"/>
        <source>Remove message</source>
        <translation>Remove message</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="1060"/>
        <source>View contact profile</source>
        <translation>View contact profile</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Conversation.qml" line="1082"/>
        <source>Load more...</source>
        <translation>Load more...</translation>
    </message>
</context>
<context>
    <name>CreateGroup</name>
    <message>
        <location filename="../UI/Groups/CreateGroup.qml" line="187"/>
        <source>Create group</source>
        <translation>Create group</translation>
    </message>
    <message>
        <location filename="../UI/Groups/CreateGroup.qml" line="212"/>
        <source>Select picture</source>
        <translation>Select picture</translation>
    </message>
    <message>
        <location filename="../UI/Groups/CreateGroup.qml" line="228"/>
        <source>Group subject</source>
        <translation>Group subject</translation>
    </message>
    <message>
        <location filename="../UI/Groups/CreateGroup.qml" line="303"/>
        <source>Group participants</source>
        <translation>Group participants</translation>
    </message>
    <message>
        <location filename="../UI/Groups/CreateGroup.qml" line="322"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
    <message>
        <location filename="../UI/Groups/CreateGroup.qml" line="364"/>
        <source>Create</source>
        <translation>Create</translation>
    </message>
</context>
<context>
    <name>Emojidialog</name>
    <message>
        <location filename="../UI/EmojiDialog/Emojidialog.qml" line="12"/>
        <source>Select Emoji</source>
        <translation>Select Emoji</translation>
    </message>
</context>
<context>
    <name>GroupProfile</name>
    <message>
        <location filename="../UI/Groups/GroupProfile.qml" line="57"/>
        <source>Save changes</source>
        <translation>Save changes</translation>
    </message>
    <message>
        <location filename="../UI/Groups/GroupProfile.qml" line="114"/>
        <location filename="../UI/Groups/GroupProfile.qml" line="271"/>
        <location filename="../UI/Groups/GroupProfile.qml" line="373"/>
        <source>You</source>
        <translation>You</translation>
    </message>
    <message>
        <location filename="../UI/Groups/GroupProfile.qml" line="139"/>
        <source>Error reading group information</source>
        <translation>Error reading group information</translation>
    </message>
    <message>
        <location filename="../UI/Groups/GroupProfile.qml" line="146"/>
        <source>Subject created by</source>
        <translation>Subject created by</translation>
    </message>
    <message>
        <location filename="../UI/Groups/GroupProfile.qml" line="147"/>
        <location filename="../UI/Groups/GroupProfile.qml" line="320"/>
        <source>Group participants:</source>
        <translation>Group participants:</translation>
    </message>
    <message>
        <location filename="../UI/Groups/GroupProfile.qml" line="271"/>
        <source>Group owner:</source>
        <translation>Group owner:</translation>
    </message>
    <message>
        <location filename="../UI/Groups/GroupProfile.qml" line="277"/>
        <source>Creation date:</source>
        <translation>Creation date:</translation>
    </message>
    <message>
        <location filename="../UI/Groups/GroupProfile.qml" line="290"/>
        <source>Change group subject</source>
        <translation>Change group subject</translation>
    </message>
    <message>
        <location filename="../UI/Groups/GroupProfile.qml" line="300"/>
        <source>Change group picture</source>
        <translation>Change group picture</translation>
    </message>
    <message>
        <location filename="../UI/Groups/GroupProfile.qml" line="340"/>
        <source>Add</source>
        <translation>Add</translation>
    </message>
</context>
<context>
    <name>LoadingPage</name>
    <message>
        <location filename="../UI/common/LoadingPage.qml" line="27"/>
        <source>Loading</source>
        <translation>Loading</translation>
    </message>
    <message>
        <location filename="../UI/common/LoadingPage.qml" line="64"/>
        <source>Sync contacts</source>
        <translation>Sync contacts</translation>
    </message>
    <message>
        <location filename="../UI/common/LoadingPage.qml" line="99"/>
        <source>Taking too long?</source>
        <translation>Taking too long?</translation>
    </message>
</context>
<context>
    <name>Location</name>
    <message>
        <location filename="../UI/Conversations/Location.qml" line="13"/>
        <location filename="../UI/Conversations/Location.qml" line="14"/>
        <source>Resolving...</source>
        <translation>Resolving...</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Location.qml" line="33"/>
        <source>Send location</source>
        <translation>Send location</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Location.qml" line="201"/>
        <source>Longitude:</source>
        <translation>Longitude:</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Location.qml" line="217"/>
        <source>Latitude:</source>
        <translation>Latitude:</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Location.qml" line="234"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
</context>
<context>
    <name>MediaBubble</name>
    <message>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="41"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="46"/>
        <source>Audio</source>
        <translation>Audio</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="51"/>
        <source>Video</source>
        <translation>Video</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="58"/>
        <source>Location</source>
        <translation>Location</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="105"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="105"/>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="216"/>
        <source>Download</source>
        <translation>Download</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="141"/>
        <source>Sending</source>
        <translation>Sending</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="141"/>
        <source>Downloading</source>
        <translation>Downloading</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="157"/>
        <source>Retry</source>
        <translation>Retry</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/MediaBubble.qml" line="220"/>
        <source>Initializing</source>
        <translation>Initializing</translation>
    </message>
</context>
<context>
    <name>MusicBrowser</name>
    <message>
        <location filename="../UI/Settings/MusicBrowser.qml" line="31"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../UI/Settings/MusicBrowser.qml" line="39"/>
        <source>Done</source>
        <translation>Done</translation>
    </message>
</context>
<context>
    <name>MusicBrowserDialog</name>
    <message>
        <location filename="../UI/common/MusicBrowserDialog.qml" line="55"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../UI/common/MusicBrowserDialog.qml" line="63"/>
        <source>Done</source>
        <translation>Done</translation>
    </message>
</context>
<context>
    <name>MusicBrowserExtended</name>
    <message>
        <location filename="../UI/common/MusicBrowserExtended.qml" line="33"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
    <message>
        <location filename="../UI/common/MusicBrowserExtended.qml" line="41"/>
        <source>Done</source>
        <translation>Done</translation>
    </message>
</context>
<context>
    <name>Profile</name>
    <message>
        <location filename="../UI/Profile/Profile.qml" line="61"/>
        <source>My Profile</source>
        <translation>My Profile</translation>
    </message>
    <message>
        <location filename="../UI/Profile/Profile.qml" line="153"/>
        <source>Change status</source>
        <translation>Change status</translation>
    </message>
    <message>
        <location filename="../UI/Profile/Profile.qml" line="166"/>
        <source>Change picture</source>
        <translation>Change picture</translation>
    </message>
</context>
<context>
    <name>SelectContacts</name>
    <message>
        <location filename="../UI/Contacts/SelectContacts.qml" line="207"/>
        <source>Select contacts</source>
        <translation>Select contacts</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/SelectContacts.qml" line="207"/>
        <source>Select contact</source>
        <translation>Select contact</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/SelectContacts.qml" line="247"/>
        <source>Quick search</source>
        <translation>Quick search</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/SelectContacts.qml" line="312"/>
        <source>No contacts yet. Try to resync</source>
        <translation>No contacts yet. Try to resync</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/SelectContacts.qml" line="376"/>
        <source>Unselect all</source>
        <translation>Unselect all</translation>
    </message>
    <message>
        <location filename="../UI/Contacts/SelectContacts.qml" line="376"/>
        <location filename="../UI/Contacts/SelectContacts.qml" line="380"/>
        <source>Select all</source>
        <translation>Select all</translation>
    </message>
</context>
<context>
    <name>SelectGroupPicture</name>
    <message>
        <location filename="../UI/Groups/SelectGroupPicture.qml" line="42"/>
        <source>Select picture</source>
        <translation>Select picture</translation>
    </message>
</context>
<context>
    <name>SelectPicture</name>
    <message>
        <location filename="../UI/Profile/SelectPicture.qml" line="42"/>
        <source>Select picture</source>
        <translation>Select picture</translation>
    </message>
</context>
<context>
    <name>SelectionItem</name>
    <message>
        <location filename="../UI/common/SelectionItem.qml" line="39"/>
        <location filename="../UI/common/SelectionItem.qml" line="60"/>
        <source>(no background)</source>
        <translation>(no background)</translation>
    </message>
</context>
<context>
    <name>SelectionItemTr</name>
    <message>
        <source>(no sound)</source>
        <translation type="obsolete">(no sound)</translation>
    </message>
</context>
<context>
    <name>SelectionItemTrSound</name>
    <message>
        <location filename="../UI/common/SelectionItemTrSound.qml" line="27"/>
        <source>(no sound)</source>
        <translation>(no sound)</translation>
    </message>
</context>
<context>
    <name>SendAudio</name>
    <message>
        <location filename="../UI/Conversations/SendAudio.qml" line="34"/>
        <source>Select audio</source>
        <translation>Select audio</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/SendAudio.qml" line="133"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/SendAudio.qml" line="144"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>SendAudioFile</name>
    <message>
        <source>Select audio</source>
        <translation type="obsolete">Select audio</translation>
    </message>
</context>
<context>
    <name>SendAudioRec</name>
    <message>
        <location filename="../UI/Conversations/SendAudioRec.qml" line="49"/>
        <source>Send</source>
        <translation>Send</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/SendAudioRec.qml" line="61"/>
        <source>Audio recorder</source>
        <translation>Audio recorder</translation>
    </message>
</context>
<context>
    <name>SendPicture</name>
    <message>
        <location filename="../UI/Conversations/SendPicture.qml" line="42"/>
        <source>Select picture</source>
        <translation>Select picture</translation>
    </message>
</context>
<context>
    <name>SendVideo</name>
    <message>
        <location filename="../UI/Conversations/SendVideo.qml" line="42"/>
        <source>Select video</source>
        <translation>Select video</translation>
    </message>
</context>
<context>
    <name>SetBackground</name>
    <message>
        <location filename="../UI/Settings/SetBackground.qml" line="42"/>
        <source>No background</source>
        <translation>No background</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SetBackground.qml" line="53"/>
        <source>Select picture</source>
        <translation>Select picture</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="78"/>
        <source>version</source>
        <translation>version</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="79"/>
        <source>This is a %1 version.</source>
        <translation>This is a %1 version.</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="80"/>
        <source>You are trying it at your own risk.</source>
        <translation>You are trying it at your own risk.</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="81"/>
        <source>Please report any bugs to</source>
        <translation>Please report any bugs to</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="86"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="145"/>
        <source>Appearance</source>
        <translation>Appearance</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="149"/>
        <source>Orientation:</source>
        <translation>Orientation:</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="154"/>
        <source>Automatic</source>
        <translation>Automatic</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="162"/>
        <source>Portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="170"/>
        <source>Landscape</source>
        <translation>Landscape</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="181"/>
        <source>Theme color:</source>
        <translation>Theme color:</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="187"/>
        <source>White</source>
        <translation>White</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="196"/>
        <source>Black</source>
        <translation>Black</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="207"/>
        <source>Bubble color:</source>
        <translation>Bubble color:</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="214"/>
        <source>Cyan</source>
        <translation>Cyan</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="222"/>
        <source>Green</source>
        <translation>Green</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="230"/>
        <source>Pink</source>
        <translation>Pink</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="238"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="248"/>
        <source>Chats</source>
        <translation>Chats</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="251"/>
        <source>Enter key sends the message</source>
        <translation>Enter key sends the message</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="260"/>
        <source>Media sending</source>
        <translation>Media sending</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="263"/>
        <source>Resize images before sending</source>
        <translation>Resize images before sending</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="273"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="276"/>
        <source>Current language</source>
        <translation>Current language</translation>
    </message>
    <message>
        <location filename="../UI/Settings/Settings.qml" line="318"/>
        <source>*Restart Wazapp to apply the new language</source>
        <translation>*Restart Wazapp to apply the new language</translation>
    </message>
</context>
<context>
    <name>SettingsNew</name>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="33"/>
        <location filename="../UI/Settings/SettingsNew.qml" line="119"/>
        <source>This is a %1 version.</source>
        <translation>This is a %1 version.</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="34"/>
        <location filename="../UI/Settings/SettingsNew.qml" line="120"/>
        <source>You are trying it at your own risk.</source>
        <translation>You are trying it at your own risk.</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="35"/>
        <location filename="../UI/Settings/SettingsNew.qml" line="121"/>
        <source>Please report any bugs to</source>
        <translation>Please report any bugs to</translation>
    </message>
    <message>
        <source>Quit Wazapp</source>
        <translation type="obsolete">Quit Wazapp</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="118"/>
        <location filename="../UI/Settings/SettingsNew.qml" line="645"/>
        <source>version</source>
        <translation>version</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="126"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="160"/>
        <source>Chats</source>
        <translation>Chats</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="163"/>
        <source>Enter key sends the message</source>
        <translation>Enter key sends the message</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="172"/>
        <source>Media sending</source>
        <translation>Media sending</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="175"/>
        <source>Resize images before sending</source>
        <translation>Resize images before sending</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="185"/>
        <source>Language</source>
        <translation>Language</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="188"/>
        <source>Current language</source>
        <translation>Current language</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="239"/>
        <source>*Restart Wazapp to apply the new language</source>
        <translation>*Restart Wazapp to apply the new language</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="276"/>
        <source>Background</source>
        <translation>Background</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="281"/>
        <source>Image</source>
        <translation>Image</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="289"/>
        <source>Opacity:</source>
        <translation>Opacity:</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="312"/>
        <source>Appearance</source>
        <translation>Appearance</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="318"/>
        <source>Orientation:</source>
        <translation>Orientation:</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="323"/>
        <source>Automatic</source>
        <translation>Automatic</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="332"/>
        <source>Portrait</source>
        <translation>Portrait</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="341"/>
        <source>Landscape</source>
        <translation>Landscape</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="353"/>
        <source>Theme color:</source>
        <translation>Theme color:</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="359"/>
        <source>White</source>
        <translation>White</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="368"/>
        <source>Black</source>
        <translation>Black</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="380"/>
        <source>Bubble color:</source>
        <translation>Bubble color:</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="387"/>
        <source>Cyan</source>
        <translation>Cyan</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="396"/>
        <source>Green</source>
        <translation>Green</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="405"/>
        <source>Pink</source>
        <translation>Pink</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="414"/>
        <source>Orange</source>
        <translation>Orange</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="468"/>
        <source>Personal messages</source>
        <translation>Personal messages</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="472"/>
        <location filename="../UI/Settings/SettingsNew.qml" line="491"/>
        <source>Notification tone</source>
        <translation>Notification tone</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="477"/>
        <location filename="../UI/Settings/SettingsNew.qml" line="497"/>
        <source>Vibrate</source>
        <translation>Vibrate</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="487"/>
        <source>Group messages</source>
        <translation>Group messages</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="547"/>
        <source>Profile picture</source>
        <translation>Profile picture</translation>
    </message>
    <message>
        <source>Change picture</source>
        <translation type="obsolete">Change picture</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="564"/>
        <source>Status</source>
        <translation>Status</translation>
    </message>
    <message>
        <source>Change current status</source>
        <translation type="obsolete">Change current status</translation>
    </message>
    <message>
        <location filename="../UI/Settings/SettingsNew.qml" line="625"/>
        <source>About</source>
        <translation>About</translation>
    </message>
</context>
<context>
    <name>StanzaReader</name>
    <message>
        <source>Image</source>
        <translation type="obsolete">Image</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="obsolete">Audio</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="obsolete">Video</translation>
    </message>
    <message>
        <source>Location</source>
        <translation type="obsolete">Location</translation>
    </message>
</context>
<context>
    <name>Status</name>
    <message>
        <location filename="../UI/Settings/Status.qml" line="141"/>
        <source>Done</source>
        <translation>Done</translation>
    </message>
</context>
<context>
    <name>TextBubble</name>
    <message>
        <location filename="../UI/Conversations/Bubbles/TextBubble.qml" line="11"/>
        <source>You</source>
        <translation>You</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/TextBubble.qml" line="25"/>
        <source>&lt;b&gt;%1&lt;/b&gt; joined the group</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; joined the group</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/TextBubble.qml" line="26"/>
        <source>&lt;b&gt;%1&lt;/b&gt; left the group</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; left the group</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/TextBubble.qml" line="28"/>
        <location filename="../UI/Conversations/Bubbles/TextBubble.qml" line="29"/>
        <source>&lt;b&gt;%1&lt;/b&gt; changed the subject to &lt;b&gt;%2&lt;/b&gt;</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; changed the subject to &lt;b&gt;%2&lt;/b&gt;</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/Bubbles/TextBubble.qml" line="31"/>
        <location filename="../UI/Conversations/Bubbles/TextBubble.qml" line="32"/>
        <source>&lt;b&gt;%1&lt;/b&gt; changed the group picture</source>
        <translation>&lt;b&gt;%1&lt;/b&gt; changed the group picture</translation>
    </message>
</context>
<context>
    <name>UserStatus</name>
    <message>
        <location filename="../UI/Conversations/UserStatus.qml" line="86"/>
        <source>Online</source>
        <translation>Online</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/UserStatus.qml" line="103"/>
        <source>Last seen:</source>
        <translation>Last seen:</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/UserStatus.qml" line="104"/>
        <source>Today</source>
        <translation>Today</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/UserStatus.qml" line="104"/>
        <source>Yesterday</source>
        <translation>Yesterday</translation>
    </message>
    <message>
        <location filename="../UI/Conversations/UserStatus.qml" line="111"/>
        <source>Typing...</source>
        <translation>Typing...</translation>
    </message>
</context>
<context>
    <name>WAEventHandler</name>
    <message>
        <source>Location</source>
        <translation type="obsolete">Location</translation>
    </message>
    <message>
        <source>Image</source>
        <translation type="obsolete">Image</translation>
    </message>
    <message>
        <source>Video</source>
        <translation type="obsolete">Video</translation>
    </message>
    <message>
        <source>Audio</source>
        <translation type="obsolete">Audio</translation>
    </message>
    <message>
        <source>%1 changed the group picture</source>
        <translation type="obsolete">%1 changed the group picture</translation>
    </message>
    <message>
        <source>%1 joined the group</source>
        <translation type="obsolete">%1 joined the group</translation>
    </message>
    <message>
        <source>%1 left the group</source>
        <translation type="obsolete">%1 left the group</translation>
    </message>
    <message>
        <source>%1 changed the subject to %2</source>
        <translation type="obsolete">%1 changed the subject to %2</translation>
    </message>
    <message>
        <source>You</source>
        <translation type="obsolete">You</translation>
    </message>
</context>
<context>
    <name>WAListView</name>
    <message>
        <location filename="../UI/common/WAListView/WAListView.qml" line="19"/>
        <source>No data</source>
        <translation>No data</translation>
    </message>
</context>
<context>
    <name>WAMenu</name>
    <message>
        <location filename="../UI/Menu/WAMenu.qml" line="37"/>
        <source>Create group</source>
        <translation>Create group</translation>
    </message>
    <message>
        <location filename="../UI/Menu/WAMenu.qml" line="54"/>
        <source>Sync Contacts</source>
        <translation>Sync Contacts</translation>
    </message>
    <message>
        <location filename="../UI/Menu/WAMenu.qml" line="66"/>
        <source>Settings</source>
        <translation>Settings</translation>
    </message>
    <message>
        <location filename="../UI/Menu/WAMenu.qml" line="78"/>
        <source>Quit</source>
        <translation>Quit</translation>
    </message>
</context>
<context>
    <name>WANotify</name>
    <message>
        <location filename="../UI/common/WANotify.qml" line="59"/>
        <source>Online</source>
        <translation>Online</translation>
    </message>
    <message>
        <location filename="../UI/common/WANotify.qml" line="73"/>
        <source>Connecting...</source>
        <translation>Connecting...</translation>
    </message>
    <message>
        <location filename="../UI/common/WANotify.qml" line="102"/>
        <source>Offline</source>
        <translation>Offline</translation>
    </message>
</context>
<context>
    <name>WAUpdate</name>
    <message>
        <location filename="../UI/Updater/WAUpdate.qml" line="130"/>
        <source>Update</source>
        <translation>Update</translation>
    </message>
</context>
<context>
    <name>main</name>
    <message>
        <location filename="../UI/main.qml" line="568"/>
        <source>Retrieving contacts list...</source>
        <translation>Retrieving contacts list...</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="570"/>
        <source>Fetching contacts...</source>
        <translation>Fetching contacts...</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="572"/>
        <source>Loading contacts...</source>
        <translation>Loading contacts...</translation>
    </message>
    <message>
        <source>Chats</source>
        <translation type="obsolete">Chats</translation>
    </message>
    <message>
        <source>Contacts</source>
        <translation type="obsolete">Contacts</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="210"/>
        <source>Browse</source>
        <translation>Browse</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="209"/>
        <source>(no sound)</source>
        <translation>(no sound)</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="904"/>
        <source>Update Contacts</source>
        <translation>Update Contacts</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="905"/>
        <source>The Phone contacts database has changed. Do you want to sync contacts now?</source>
        <translation>The Phone contacts database has changed. Do you want to sync contacts now?</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="906"/>
        <location filename="../UI/main.qml" line="915"/>
        <source>Yes</source>
        <translation>Yes</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="907"/>
        <location filename="../UI/main.qml" line="916"/>
        <source>No</source>
        <translation>No</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="913"/>
        <source>Confirm Quit</source>
        <translation>Confirm Quit</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="914"/>
        <source>Are you sure you want to quit Wazapp?</source>
        <translation>Are you sure you want to quit Wazapp?</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="924"/>
        <source>version</source>
        <translation>version</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="925"/>
        <source>This is a %1 version.</source>
        <translation>This is a %1 version.</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="926"/>
        <source>You are trying it at your own risk.</source>
        <translation>You are trying it at your own risk.</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="927"/>
        <source>Please report any bugs to</source>
        <translation>Please report any bugs to</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="938"/>
        <source>Wazapp %1 is now available for update!</source>
        <translation>Wazapp %1 is now available for update!</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="940"/>
        <source>Details</source>
        <translation>Details</translation>
    </message>
    <message>
        <location filename="../UI/main.qml" line="941"/>
        <source>Cancel</source>
        <translation>Cancel</translation>
    </message>
</context>
<context>
    <name>ringtoneModel</name>
    <message>
        <source>(no sound)</source>
        <translation type="obsolete">(no sound)</translation>
    </message>
</context>
</TS>
